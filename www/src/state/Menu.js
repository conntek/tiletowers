var TileTowers = TileTowers || {};

TileTowers.Menu = function (game) {
};

TileTowers.Menu.prototype.init = function() {
    this.heroData = [];


    var arcaneWarrior = {
        name : 'Arcane Warrior',
        selectKey : 'arcane_warrior_select',
        key : 'arcane_warrior',
        health : 10,
        attack : 2,
        defence : 0,
        init : 2,
        dodge : 5,
        block : 0,
        crit : 5,
        level : 1,
        xp : 100
    };

    var awData = localStorage.getItem("arcane_warrior");

    if (awData) {
        awData = JSON.parse(awData);
        TileTowers.LevelUp.setLevel(arcaneWarrior.key, awData.level, arcaneWarrior);
        arcaneWarrior.xp = awData.xp;
    }


    this.heroData.push(arcaneWarrior);

    this.towerData = [];

    this.towerData.push(
        {
            name: 'Orc Tower 1',
            key : 'orc1',
            level : 1
        }
    );

    this.towerData.push(
        {
            name: 'Orc Tower 2',
            key : 'orc2',
            level : 1
        }
    );


    this.selectedHero = undefined;
    this.selectedTower = undefined;


    this.clickSound = this.game.add.audio('click');
};

TileTowers.Menu.prototype.create = function() {

    var background = this.game.add.sprite(0, 0, 'background');
    background.width = this.game.world.width;
    background.height = this.game.world.height;

    var titleText = this.add.text(this.game.world.width/2, 100, 'TILE TOWERS', {font :'bold 80px sans-serif', fill : '#fff'});
    titleText.x = Math.round(this.game.world.width/2 - titleText.width/2);
    titleText.y = Math.round(100 - titleText.height/2);
    titleText.stroke = '#666';
    titleText.strokeThickness = 4;
    titleText.setShadow(5, 5, 'rgba(0,0,0,0.5)', 15);

    this.selectText = this.add.text(this.game.world.width/2, 200, 'Choose your hero:', {font :'bold 30px sans-serif', fill : '#fff'});
    this.selectText.x = Math.round(this.game.world.width/2 - this.selectText.width/2);
    this.selectText.y = Math.round(200 - this.selectText.height/2);
    this.selectText.stroke = '#000000';
    this.selectText.strokeThickness = 0;

    // loop

    this.heroGroup = this.add.group();
    this.heroData.forEach(function(heroData, i) {
        var heroSprite = this.add.sprite(i * 256, 300, heroData.selectKey, 0);
        heroSprite.animations.add('idle', [0,1,2,3,4], 5, true);
        heroSprite.animations.play('idle');

        heroSprite.inputEnabled = true;
        heroSprite.events.onInputDown.add(function() {this.heroSelect(heroData);}, this);

        var heroName = this.add.text(128 + i * 256, 570, heroData.name, {font :'bold 26px Anton', fill : '#fff'});
        heroName.x = Math.round(heroName.x - heroName.width/2);
        heroName.y = Math.round(heroName.y - heroName.height/2);
        heroName.stroke = '#000000';
        heroName.strokeThickness = 0;

        var heroLevel = this.add.text(128 + i * 256, 600, 'Level '+heroData.level, {font :'bold 22px Anton', fill : '#fff'});
        heroLevel.x = Math.round(heroLevel.x - heroLevel.width/2);
        heroLevel.y = Math.round(heroLevel.y - heroLevel.height/2);


        this.heroGroup.add(heroSprite);
        this.heroGroup.add(heroName);
        this.heroGroup.add(heroLevel);
    }, this);

};

TileTowers.Menu.prototype.heroSelect = function(heroData) {
    this.clickSound.play();
    this.selectedHero = heroData;

    this.heroPanel = this.add.group();

    var gameOverlay = this.add.bitmapData(this.game.world.width, this.game.world.height);
    gameOverlay.ctx.fillStyle = '#000';
    gameOverlay.ctx.fillRect(0, 0, this.game.world.width, this.game.world.height);

    var mask = this.add.sprite(0, 0, gameOverlay);
    mask.alpha = 0.4;
    mask.inputEnabled = true;

    this.heroPanel.add(mask);


    var panel = this.add.sprite(this.game.width/2, this.game.height/2, 'panel');
    panel.width = 600;
    panel.height = 600;
    panel.anchor.setTo(0.5);

    this.heroPanel.add(panel);

    var selectText = this.add.text(this.game.width/2, 120, heroData.name, {font :' 32px Anton', fill : '#fff'});
    selectText.x = Math.round(selectText.x - selectText.width/2);
    selectText.y = Math.round(selectText.y - selectText.height/2);

    this.heroPanel.add(selectText);

    var sprite = this.add.sprite(this.game.width/2, 220, heroData.key, 0);
    sprite.anchor.set(0.5);
    sprite.animations.add('attack', [0,2,3,4,5,0,0,0,0,0,0], 10, true);
    sprite.play('attack');

    this.heroPanel.add(sprite);

    var cancelBtn = this.add.sprite(740, 80, 'cancel');
    cancelBtn.width = 32;
    cancelBtn.height = 32;
    cancelBtn.inputEnabled=true;
    cancelBtn.events.onInputDown.add(this.closeHeroSelect, this);

    this.heroPanel.add(cancelBtn);

    var button = this.add.button(this.game.world.width/2, 540, 'btn_lg', this.towerSelect, this, 1, 0, 1, 0);
    button.anchor.setTo(0.5);

    this.heroPanel.add(button);

    var buttonText = this.add.text(this.game.world.width/2, 540, 'Select', {font :' 28px Anton', fill : '#fff'});
    buttonText.x = Math.round(buttonText.x - buttonText.width/2);
    buttonText.y = Math.round(buttonText.y - buttonText.height/2);

    this.heroPanel.add(buttonText);

};

TileTowers.Menu.prototype.closeHeroSelect = function() {
    this.clickSound.play();
    this.heroPanel.destroy(true);
};

TileTowers.Menu.prototype.towerSelect = function() {
    this.closeHeroSelect();
    this.heroGroup.destroy(true);

    this.selectText.text = "Choose a tower:";
    this.selectText.x = Math.round(this.game.world.width/2 - this.selectText.width/2);
    this.selectText.y = Math.round(200 - this.selectText.height/2);

    this.towerData.forEach(function(towerData, i) {
        var sprite = this.add.sprite(128+i * 256, 300, towerData.key, 0);
        sprite.anchor.setTo(0.5);

        sprite.inputEnabled = true;
        sprite.events.onInputDown.add(function() {this.clickSound.play();this.selectedTower = towerData; this.startGame();}, this);

        var name = this.add.text(128 + i * 256, 400, towerData.name, {font :'bold 26px Anton', fill : '#fff'});
        name.x = Math.round(name.x - name.width/2);
        name.y = Math.round(name.y - name.height/2);
    }, this);
};

TileTowers.Menu.prototype.startGame = function() {
    this.state.start('Game', true, false, this.selectedHero, this.selectedTower);
};
