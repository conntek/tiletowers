var TileTowers = TileTowers || {};

TileTowers.Preload  = function (game) {
};


TileTowers.Preload.prototype = {
    preload: function () {

        this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloadbar');
        this.preloadBar.anchor.setTo(0.5);
        this.preloadBar.scale.setTo(3);

        this.load.image('background', 'asset/image/background.png');
        this.load.image('slot', 'asset/image/slot.png');

        // ui
        this.load.image('panel', 'asset/image/panel.png');
        this.load.spritesheet('btn_lg', 'asset/image/btn_lg.png', 399, 103, 2, 0, 0);
        this.load.image('cancel', 'asset/image/cancel.png');

        this.load.image('stairs', 'asset/image/stairs.png');
        this.load.image('blocked', 'asset/image/blocked.png');
        this.load.image('cover', 'asset/image/cover.png');
        this.load.image('floor', 'asset/image/floor.png');

        this.load.image('health', 'asset/image/health.png');
        this.load.image('attack', 'asset/image/attack.png');
        this.load.image('defence', 'asset/image/defence.png');

        this.load.spritesheet('arcane_warrior_select', 'asset/image/arcane_warrior_idle.png', 256, 256, 5, 0, 0);
        this.load.spritesheet('arcane_warrior', 'asset/image/arcane_warrior_ss.png', 128, 128, 11, 0, 0);

        this.load.spritesheet('orc1', 'asset/image/orc1_ss.png', 128, 128, 11, 0, 0);
        this.load.spritesheet('orc2', 'asset/image/orc2_ss.png', 128, 128, 11, 0, 0);


        this.load.image('potion_health', 'asset/image/potion_health.png');
        this.load.image('attack_up', 'asset/image/attack_up.png');
        this.load.image('defence_up', 'asset/image/defence_up.png');





        this.load.audio('hit', 'asset/audio/hit.mp3');
        this.load.audio('stairs', 'asset/audio/stairs.mp3');
        this.load.audio('click', 'asset/audio/click.mp3');
        this.load.audio('drink', 'asset/audio/drink.mp3');
        this.load.audio('levelup', 'asset/audio/levelup.mp3');
    },
    create: function() {
        this.state.start('Menu');
    }
};