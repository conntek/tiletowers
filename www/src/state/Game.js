var TileTowers = TileTowers || {};

TileTowers.TILE_SIZE = 128;
TileTowers.BOARD_SIZE_X = 6;
TileTowers.BOARD_SIZE_Y = 5;

TileTowers.Game = function (game) {
};

// set Game function prototype
TileTowers.Game.prototype = {

    init: function (playerData, towerData) {

        this.playerData = playerData;
        this.towerData = towerData;
    },

    create: function () {

        var background = this.game.add.sprite(0, 0, 'background');
        background.width = this.game.world.width;
        background.height = this.game.world.height;

        this.player = new TileTowers.Player(this, 0, 40, this.playerData);
        this.game.add.existing(this.player);


        this.board = new TileTowers.Board(this, 256, 40, this.towerData);

        this.add.text(20, 5, 'Tower Level : ' + this.towerData.level, {font :'bold 26px Anton', fill : '#fff'});

        this.sounds = {
            stairs : this.game.add.audio('stairs'),
            drink : this.game.add.audio('drink'),
            levelUp : this.game.add.audio('levelup'),
            hit : this.game.add.audio('hit'),
            click : this.game.add.audio('click')
        };

    },

    update: function () {

    },

    render: function () {
        //this.game.debug.body(this.player);
        //this.game.debug.bodyInfo(this.player, 0, 20);
    },

    nextLevel : function() {
        this.sounds.stairs.play();
        this.towerData.level += 1;
        this.game.state.start('Game', true, false, this.player.data, this.towerData);
    },

    gameOver : function() {
        this.gameOverlay = this.add.bitmapData(this.game.world.width, this.game.world.height);
        this.gameOverlay.ctx.fillStyle = '#000';
        this.gameOverlay.ctx.fillRect(0, 0, this.game.world.width, this.game.world.height);

        this.panel = this.add.sprite(0, 0, this.gameOverlay);
        this.panel.alpha = 0.1;

        this.panel.inputEnabled = true;


        var panelAnim = this.game.add.tween(this.panel);
        panelAnim.to({alpha:0.6}, 1000);
        panelAnim.start();

        var style = {font :'bold 60px Anton', fill : '#fff'};
        var diedText = this.add.text(Math.round(this.game.world.width/2), Math.round(this.game.world.height/2), 'YOU DIED!', style);
        diedText.x = diedText.x - diedText.width/2;
        diedText.y = diedText.y - diedText.height/2;
        diedText.stroke = '#000000';
        diedText.strokeThickness = 4;

        panelAnim.onComplete.add(function() {

            localStorage.setItem(this.playerData.key, JSON.stringify(this.playerData));

            var clickText = this.add.text(Math.round(this.game.world.width/2), Math.round(this.game.world.height/2+100), 'NEXT', {font :'24px Anton', fill : '#fff'});
            clickText.x = clickText.x - clickText.width/2;
            clickText.y = clickText.y - clickText.height/2;
            clickText.stroke = '#000000';
            clickText.strokeThickness = 3;

            this.panel.events.onInputDown.add(function() {
                this.game.state.start('Menu', true, false);
            }, this);
        }, this);
    }
};