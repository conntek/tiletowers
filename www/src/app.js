var TileTowers = TileTowers || {};

(function () {

    TileTowers.game = new Phaser.Game(1024, 682, Phaser.AUTO);

    TileTowers.game.state.add('Boot', TileTowers.Boot);
    TileTowers.game.state.add('Preload', TileTowers.Preload);
    TileTowers.game.state.add('Menu', TileTowers.Menu);
    TileTowers.game.state.add('Game', TileTowers.Game);
    TileTowers.game.state.add('GameOver', TileTowers.GameOver);

    TileTowers.game.state.start('Boot');

})();