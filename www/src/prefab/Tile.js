var TileTowers = TileTowers || {};


TileTowers.Tile = function(board, state, x, y, item, enemy, exit) {

    this.board = board;
    this.state = state;

    this.open = false;
    this.canOpen = true;


    this.floor = new Phaser.Sprite(this.state.game, x, y, 'floor');
    this.floor.width = TileTowers.TILE_SIZE;
    this.floor.height = TileTowers.TILE_SIZE;
    this.state.game.add.existing(this.floor);

    if (exit) {
        this.stairs = new Phaser.Sprite(this.state.game, x, y, 'stairs');
        this.stairs.width = TileTowers.TILE_SIZE;
        this.stairs.height = TileTowers.TILE_SIZE;
        this.state.game.add.existing(this.stairs);
        this.stairs.inputEnabled = true;
        this.stairs.events.onInputDown.add(this.takeStairs, this);
    }


    if (item) {
        // item
        this.item = new TileTowers.Item(this.state.game, x, y, item);
        this.state.game.add.existing(this.item);
        this.item.inputEnabled = true;
        this.item.events.onInputDown.add(this.pickUpItem, this);
    }
    if (enemy) {
        // enemy
        this.enemy = new TileTowers.Enemy(this.state, x, y, enemy);
        this.enemy.sprite.inputEnabled = true;
        this.enemy.sprite.events.onInputDown.add(this.battle, this);
    }

    this.cover = new Phaser.Sprite(this.state.game, x, y, 'cover');
    this.cover.width = TileTowers.TILE_SIZE;
    this.cover.height = TileTowers.TILE_SIZE;
    this.state.game.add.existing(this.cover);
    this.cover.inputEnabled = true;
    this.cover.events.onInputDown.add(function() {this.openTile(true);}, this);

    this.blockedSprite = new Phaser.Sprite(this.state.game, x, y, 'blocked');
    this.blockedSprite.width = TileTowers.TILE_SIZE;
    this.blockedSprite.height = TileTowers.TILE_SIZE;
    this.state.game.add.existing(this.blockedSprite);
    this.blockedSprite.inputEnabled = true;
    this.updateBlockedSprite();

};

TileTowers.Tile.prototype.openTile = function(cascade) {
    if (this.canOpen) {
        this.state.sounds.click.play();
        this.cover.kill();
        this.open = true;

        if (this.enemy) {
            // block tiles
            this.board.updateBlockedTiles();
        }

        if (this.isEmpty() && cascade) {
            this.board.openEmptyTiles(this);
        }
    }
};

TileTowers.Tile.prototype.updateBlockedSprite = function() {

    this.blockedSprite.visible = !this.open && this.blocked;

};

TileTowers.Tile.prototype.battle = function() {
    TileTowers.Battle.resolve(this.state.player, this.enemy);
    // update blocked tiles
    this.board.updateBlockedTiles();
};

TileTowers.Tile.prototype.pickUpItem = function() {

    if (this.item.data.consumable) {
        this.state.player.consume(this.item);
    } else if (this.item.data.equipable) {
        this.state.player.equip(this.item);
    }
};

TileTowers.Tile.prototype.enemyVisible = function() {
    return this.open && this.enemy && this.enemy.alive;
};

TileTowers.Tile.prototype.isEmpty = function() {
    return !this.enemy && !this.item && !this.stairs;
};

TileTowers.Tile.prototype.takeStairs = function() {
    this.state.nextLevel();
};
