var TileTowers = TileTowers || {};

TileTowers.Battle = {};

TileTowers.Battle.resolve = function(player, enemy) {
    var playerInit = Math.random()*10 + player.data.init;
    var enemyInit = Math.random()*10 + enemy.data.init;

    if (playerInit >= enemyInit) {

        this.attack(player, enemy);

        if (enemy.alive) {
            enemy.hit();
            this.attack(enemy, player);
        }

    } else {

        this.attack(enemy, player);
        enemy.attack();

        if (player.alive) {
            this.attack(player, enemy);
        }
    }

    if (player.alive) {
        player.attack();
    }

    if (!enemy.alive) {
        player.gainXp(enemy.data.level*10+enemy.data.level);
    }
};

TileTowers.Battle.attack = function(attacker, defender) {

    if (Math.random()*100 < defender.data.dodge) {
        defender.showText('Dodged!');
        return;
    }

    if (Math.random()*100 < defender.data.block) {
        defender.showText('Blocked!');
        return;
    }

    var crit = Math.random()*100 < attacker.data.crit;

    var attack = crit ? attacker.data.attack * 2 : attacker.data.attack;

    var damage = attack - defender.data.defence;
    if (damage < 1) {
        damage = 1;
    }

    defender.takeDamage(damage);
};