var TileTowers = TileTowers || {};

TileTowers.Enemy = function(state, x, y, data) {

    this.x = x + TileTowers.TILE_SIZE/2;
    this.y = y + TileTowers.TILE_SIZE/2;
    this.game = state.game;
    this.state = state;
    this.data = data;
    this.alive = true;

    this.sprite = this.game.add.sprite(this.x, this.y, data.key, 0);
    this.sprite.width = TileTowers.TILE_SIZE;
    this.sprite.height = TileTowers.TILE_SIZE;
    this.sprite.anchor.setTo(0.5);
    this.sprite.scale.setTo(-1, 1);

    this.sprite.animations.add('hit', [1,0], 10, false);
    this.sprite.animations.add('attack', [2,3,4,5,0], 20, false);
    this.sprite.animations.add('die', [6,7,8,9,10], 15, false);


    var style = {font : '16px Anton', fill: '#fff'};

    this.healthIcon = this.game.add.sprite(x+8, y+1, 'health');
    this.healthIcon.width = 17;
    this.healthIcon.height = 17;

    this.healthText = this.game.add.text(x+26, y - 2, this.data.health, style);
    this.healthText.stroke = '#000';
    this.healthText.strokeThickness = 5;

    this.attackIcon = this.game.add.sprite(x + 48, y+1, 'attack');
    this.attackIcon.width = 17;
    this.attackIcon.height = 17;

    this.attackText = this.game.add.text(x + 66, y-2, this.data.attack, style);
    this.attackText.stroke = '#000';
    this.attackText.strokeThickness = 5;

    this.defenceIcon = this.game.add.sprite(x + 86, y+1, 'defence');
    this.defenceIcon.width = 17;
    this.defenceIcon.height = 17;
    //
    this.defenceText = this.game.add.text(x + 104, y-2, this.data.defence, style);
    this.defenceText.stroke = '#000';
    this.defenceText.strokeThickness = 5;

    this.battleText = this.game.add.text(this.sprite.x, y, '', {font : 'bold 28px Anton', fill: '#f00'});
    this.battleText.stroke = '#000000';
    this.battleText.strokeThickness = 3;
};

TileTowers.Enemy.prototype.kill = function() {

    var deathTween = this.game.add.tween(this.sprite).to( { alpha: 0.1}, 500, "Linear", true);

    deathTween.onComplete.add(function() {
        this.sprite.kill();
        this.healthIcon.kill();
        this.healthText.destroy();
        this.attackIcon.kill();
        this.attackText.destroy();
        this.defenceIcon.kill();
        this.defenceText.destroy();
        this.battleText.destroy();
    }, this);

};

TileTowers.Enemy.prototype.attack = function() {
    this.sprite.play('attack');
};

TileTowers.Enemy.prototype.hit = function() {
    this.sprite.play('hit');
};

TileTowers.Enemy.prototype.takeDamage = function(damage) {

    this.state.sounds.hit.play();

    this.data.health -= damage;
    this.showText('-'+damage);

    if (this.data.health < 1) {
        this.healthText.text = 0;

        this.sprite.animations.stop();
        this.sprite.play('die');
        this.sprite.inputEnabled = false;
        this.alive = false;
        this.game.time.events.add(500, this.kill, this);
    } else {
        this.healthText.text = this.data.health;
    }
};

TileTowers.Enemy.prototype.showText = function(text) {
    this.battleText.text = text;

    this.battleText.anchor.x = Math.round(this.battleText.width * 0.5) / this.battleText.width;
    this.battleText.y = this.y;
    this.battleText.alpha = 1;
    this.game.add.tween(this.battleText).to( { alpha: 0 , y: this.y-60}, 1000, "Linear", true);
};

