var TileTowers = TileTowers || {};

TileTowers.Item = function(game, x, y, data) {
    Phaser.Sprite.call(this, game, x, y, data.key);

    this.data = data;
    this.height = TileTowers.TILE_SIZE;
    this.width = TileTowers.TILE_SIZE;
};

TileTowers.Item.prototype = Object.create(Phaser.Sprite.prototype);
TileTowers.Item.prototype.constructor = TileTowers.Item;
