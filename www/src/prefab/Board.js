var TileTowers = TileTowers || {};


TileTowers.Board = function(state, x, y, towerData) {
    this.state = state;

    this.board = [];
    var exitI = Math.floor(Math.random()*TileTowers.BOARD_SIZE_X), exitJ = Math.floor(Math.random()*TileTowers.BOARD_SIZE_Y);

    var i, j, tile, item, enemy, exit, random;

    for (i = 0; i < TileTowers.BOARD_SIZE_X; i++) {
        for (j = 0; j <TileTowers.BOARD_SIZE_Y; j++) {

            item = null;
            enemy = null;
            exit = false;


            if (i == exitI && j == exitJ) {
                console.log('exit ' +i + ' ' + j);
                exit = true;
            } else {
                random = Math.random();

                if (random < 0.05) {
                    item = {
                        key : 'potion_health',
                        consumable : true,
                        sound : 'drink',
                        health : 2+Math.floor(towerData.level/2)
                    };
                }

                else if (random < 0.1) {
                    item = {
                        key : 'attack_up',
                        consumable : true,
                        attack : 1
                    };
                }

                else if (random < 0.15) {
                    item = {
                        key : 'defence_up',
                        consumable : true,
                        defence : 1
                    };
                }

                else if (0.9 > random && random > 0.7) {
                    enemy = {
                        key : 'orc1',
                        health: 2+towerData.level,
                        level : 1,
                        attack : 1,
                        defence : 1,
                        init : 1,
                        dodge : 0,
                        block : 0,
                        crit : 0
                    }
                } else if (random >= 0.9) {
                        enemy = {
                            key : 'orc2',
                            level : 1,
                            health: 2,
                            attack : 1+towerData.level,
                            defence : 0,
                            init : 1,
                            dodge : 0,
                            block : 0,
                            crit : 0
                    }
                }

            }

            tile = new TileTowers.Tile(this, this.state, x + i * TileTowers.TILE_SIZE, y+ j * TileTowers.TILE_SIZE, item, enemy, exit);

            if (!this.board[i]) {
                this.board[i] = [];
            }
            this.board[i][j] = tile;
        }
    }
};

TileTowers.Board.prototype.updateBlockedTiles = function() {
    var i, j;

    for (i = 0; i < TileTowers.BOARD_SIZE_X; i++) {
        for (j = 0; j < TileTowers.BOARD_SIZE_Y; j++) {
            this.board[i][j].blocked =false;
        }
    }

    for (i = 0; i < TileTowers.BOARD_SIZE_X; i++) {
        for (j = 0; j < TileTowers.BOARD_SIZE_Y; j++) {

            if (this.board[i][j].enemyVisible()) {
                this.getNeighbours(i, j).forEach(function(tile) {
                    tile.blocked = true;
                }, this);
            }
        }
    }

    for (i = 0; i < TileTowers.BOARD_SIZE_X; i++) {
        for (j = 0; j < TileTowers.BOARD_SIZE_Y; j++) {
            this.board[i][j].updateBlockedSprite();
        }
    }
};

TileTowers.Board.prototype.openEmptyTiles = function(tile) {
    for (i = 0; i < TileTowers.BOARD_SIZE_X; i++) {
        for (j = 0; j < TileTowers.BOARD_SIZE_Y; j++) {

            if (this.board[i][j] == tile) {
                this.getNeighbours(i,j, true).forEach(function(t) {
                    if (t.isEmpty() && !t.open && !t.blocked) {
                        t.openTile();
                    }
                }, this);
            }
        }
    }
};

TileTowers.Board.prototype.getNeighbours = function(i, j, excludeDiagonal) {
    var neighbours = [];


    if (this.board[i][j-1]) {
        neighbours.push(this.board[i][j-1]);
    }
    if (this.board[i-1] && this.board[i-1][j]) {
        neighbours.push(this.board[i-1][j]);
    }

    if (this.board[i][j+1]) {
        neighbours.push(this.board[i][j+1]);
    }
    if (this.board[i+1] && this.board[i+1][j]) {
        neighbours.push(this.board[i+1][j]);
    }


    if(!excludeDiagonal) {
        if (this.board[i-1] && this.board[i-1][j-1]) {
            neighbours.push(this.board[i-1][j-1]);
        }

        if (this.board[i+1] && this.board[i+1][j+1]) {
            neighbours.push(this.board[i+1][j+1]);
        }

        if (this.board[i+1] && this.board[i+1][j-1]) {
            neighbours.push(this.board[i+1][j-1]);
        }

        if (this.board[i-1] && this.board[i-1][j+1]) {
            neighbours.push(this.board[i-1][j+1]);
        }
    }

    return neighbours;
};