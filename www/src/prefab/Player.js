var TileTowers = TileTowers || {};

TileTowers.Player = function(state, x, y, data) {
    Phaser.Sprite.call(this, state.game, x, y, data.key, 0);

    this.state = state;

    this.width = TileTowers.TILE_SIZE;
    this.height = TileTowers.TILE_SIZE;

    this.data = data;

    this.animations.add('hit', [1,0], 10, false);
    this.animations.add('attack', [2,3,4,5,0], 20, false);
    this.animations.add('die', [6,7,8,9,10], 15, false);

    var style = {font : 'bold 22px Anton', fill: '#fff'};

    var healthIcon = this.game.add.sprite(x + 130, y+20, 'health');
    healthIcon.width = 24;
    healthIcon.height = 24;

    this.healthText = this.game.add.text(x + 160, y+22, this.data.health, style);

    var attackIcon = this.game.add.sprite(x + 130, y+52, 'attack');
    attackIcon.width = 24;
    attackIcon.height = 24;

    this.attackText = this.game.add.text(x + 160, y+52, this.data.attack, style);

    var defenceIcon = this.game.add.sprite(x + 130, y+84, 'defence');
    defenceIcon.width = 24;
    defenceIcon.height = 24;

    this.defenceText = this.game.add.text(x + 160, y+84, this.data.defence, style);

    this.levelMtr = this.game.add.sprite(10, 190, 'preloadbar');
    this.levelMtr.scale.setTo(3);
    this.levelMtr.width = 230;

    this.updateStats();




    this.inventory = new TileTowers.Inventory(this.state);

};

TileTowers.Player.prototype = Object.create(Phaser.Sprite.prototype);
TileTowers.Player.prototype.constructor = TileTowers.Player;


TileTowers.Player.prototype.updateStats = function() {
    this.healthText.text = this.data.health;
    this.attackText.text = this.data.attack;
    this.defenceText.text = this.data.defence;

    var percent = 1 - (this.data.xp / (this.data.level * 100));
    this.levelMtr.width = 240 * percent;
};

TileTowers.Player.prototype.consume = function(item) {

    if (item.data.health) {
        this.data.health += item.data.health;
        this.showText('+'+item.data.health);
    }

    if (item.data.attack) {
        this.data.attack += item.data.attack;
    }

    if (item.data.defence) {
        this.data.defence += item.data.defence;
    }


    if (item.data.sound) {
        this.state.sounds[item.data.sound].play();
    }

    item.kill();


    this.updateStats();
};

TileTowers.Player.prototype.equip = function(item) {
    this.inventory.addItem(item);
};

TileTowers.Player.prototype.takeDamage = function(damage) {
    this.data.health -= damage;
    this.showText('-'+damage);
    if (this.data.health < 1) {
        // game over
        this.data.health = 0;
        this.die();
    }
    this.updateStats();

};

TileTowers.Player.prototype.showText = function(text) {
    if (!this.battleText) {
        this.battleText = this.game.add.text(-100, -100, '', {font : 'bold 28px Anton', fill: '#f00'});
        this.battleText.x = this.x + this.width/2;
        this.battleText.stroke = '#000000';
        this.battleText.strokeThickness = 3;
    }

    this.battleText.text = text;

    if (text[0] == '+') {
        this.battleText.fill = '#0f0';
    } else if (text[0] == '-') {
        this.battleText.fill = '#f00';
    } else {
        this.battleText.fill = '#fff';
    }

    this.battleText.anchor.x = Math.round(this.battleText.width * 0.5) / this.battleText.width;
    this.battleText.y = this.y + this.height/3;
    this.battleText.alpha = 1;
    this.game.add.tween(this.battleText).to( { alpha: 0 , y: this.y-50}, 1500, "Linear", true);
};

TileTowers.Player.prototype.attack = function() {
    this.play('attack');
};

TileTowers.Player.prototype.hit = function() {
    this.play('hit');
};


TileTowers.Player.prototype.die = function() {
    this.animations.stop();
    this.alive = false;
    this.play('die');

    this.state.gameOver();
};

TileTowers.Player.prototype.gainXp = function(xp) {
    this.data.xp -= xp;

    if (this.data.xp <= 0) {
        this.levelUp();
    }

    this.updateStats();
};


TileTowers.Player.prototype.levelUp = function() {

    this.state.sounds.levelUp.play();
    this.showText("Level UP!");

    TileTowers.LevelUp.levelUp(this.data.key, this.data);

    this.updateStats();
};
