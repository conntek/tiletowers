var TileTowers = TileTowers || {};

TileTowers.LevelUp = {

    levelUpStats: {
        'arcane_warrior': {
            healthUp : 2,
            attackUp : 1,
            defenceUp : 2,
            dodgeUp : 2,
            blockUp : 0,
            critUp : 2
        }
    }

};

TileTowers.LevelUp.levelUp = function(key, data) {
    data.level += 1;

    this.applyStats(this.levelUpStats[key], data);
};

TileTowers.LevelUp.applyStats = function(stats, data) {

        data.health += stats.healthUp;

        if (data.level % stats.attackUp == 0) {
            data.attack++;
        }

        if (data.level % stats.defenceUp == 0) {
            data.defence++;
        }

        if (data.level % stats.initUp == 0) {
            data.init++;
        }

        if (data.level % stats.dodgeUp == 0) {
            data.dodge++;
        }

        if (data.level % stats.blockUp == 0) {
            data.block++;
        }

        if (data.level % stats.critUp == 0) {
            data.crit++;
        }

        data.xp = data.level * 100;

};

TileTowers.LevelUp.setLevel = function(key, level, data) {

    while(data.level < level) {
        this.levelUp(key, data);
    }

};