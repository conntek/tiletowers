var TileTowers = TileTowers || {};


TileTowers.Inventory = function(state) {
    this.state = state;

    this.data = [null, null, null, null, null, null];
    this.slots = [];

    this.slots.push(this.state.add.sprite(0, 256, 'slot'));
    this.slots.push(this.state.add.sprite(128, 256, 'slot'));
    this.slots.push(this.state.add.sprite(0, 384, 'slot'));
    this.slots.push(this.state.add.sprite(128, 384, 'slot'));
    this.slots.push(this.state.add.sprite(0, 512, 'slot'));
    this.slots.push(this.state.add.sprite(128, 512, 'slot'));

};


TileTowers.Inventory.prototype.addItem = function(item) {

    var index = this.nextSlot();

    if (index >= 0) {
        this.data[index] = item.data;
        item.x = this.slots[index].x;
        item.y = this.slots[index].y;
    }

};

TileTowers.Inventory.prototype.nextSlot = function() {

    var i;

    for (i=0; i < 6; i++) {
        if (!this.data[i]) {
            return i;
        }
    }

    return -1;
};